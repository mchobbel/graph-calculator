package org.qualifair.gcalc;



import org.qualifair.parser.*;

import java.io.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.util.*;
import java.util.stream.Collectors;
import org.javatuples.*;

public class OperatorObj {

    List<Pair<String,Double>> data = new ArrayList<Pair<String,Double>>();
    Stack<Instruction> ilist = new Stack<Instruction>();
    Stack<Instruction> stack = new Stack<Instruction>();
    String targetVar = "";

    class ArithmeticBaseListener implements ParseTreeListener {
	public void exitEveryRule(ParserRuleContext ctx){
	}
	public void enterEveryRule(ParserRuleContext ctx){
	}
	public void visitErrorNode(ErrorNode en){
	}
	public void visitTerminal(TerminalNode tnode){
	    Token symbol = tnode.getSymbol();
	    String name = symbol.getText();
	    if(name.equals(")")) return;
	    if(name.equals("(")) return;
	    if(name.equals(",")) return;
	    if(name.equals("=")) return;
	    if(name.equals("INT(")) return;
	    if(name.equals("FLOAT(")) return;
	    //System.out.println(" terminal "+name);

	    ParseTree parent = tnode.getParent();
	    
	    if(parent instanceof ParserRuleContext ){
		int idx = ((ParserRuleContext)parent).getRuleIndex();
		String pname = GcalcParser.ruleNames[idx];
		//System.out.println(" pname: "+pname);
		if(pname.equals("integer")|| pname.equals("floating")){
		    Instruction i = new Instruction(pname,"constant");
		    i.value=Double.valueOf(name);
		    ilist.push(i);
		}else{
		    ilist.push(new Instruction(name,pname));
		}
	    }

	}
	
    }

	

    public void putInput(String key, String value){
	Double dval = Double.parseDouble(value);
	data.add(new Pair(key,dval));
    }
    public void putInputD(String key, Double dval){

	data.add(new Pair(key,dval));
    }
    public void showInput(){
	System.out.println("input data");
	data.forEach((p)->System.out.println("  - " + p.getValue0() + " : " +p.getValue1()));
    }
    public Double getInput(String key){
	for(Pair<String,Double> p : data){
	    if(p.getValue0().equals(key)){
		return p.getValue1();
	    }
	}
	return null;
    }

    void init(String input_s){
	ANTLRInputStream is = new ANTLRInputStream(input_s);
	GcalcLexer  lexer = new GcalcLexer(is);
	CommonTokenStream ts = new CommonTokenStream(lexer);
	GcalcParser parser =  new GcalcParser(ts);
	parser.setErrorHandler(new BailErrorStrategy());
	ParserRuleContext ctx = parser.r();
	ParseTreeWalker	walker = new ParseTreeWalker();
	walker.walk(new ArithmeticBaseListener(),ctx);

	Instruction trg = ilist.get(0);

	this.targetVar = trg.name.toString();
	ilist.remove(0);
    }

    void printIlist(){
	System.out.println("Gcalc: i-list");
	for(Instruction x: ilist){
	    System.out.println("Gcalc i: "+x.toString());
	}
    }

    // implement a stack machine  see:
    // https://medium.com/@dmytro.anokhin/parsing-and-evaluating-expressions-using-antlr-in-swift-397609b1f7d4
    
    Double execute() throws InstrException{
	List<Instruction> list = (List<Instruction>)ilist.clone();
	Collections.reverse(list);
	/*
	System.out.println("ilist summary:");
	for(Instruction i:list){
	    System.out.println(" i: "+i);
	}
	*/

	for(Instruction i:list){
	    if(i.instr == Instr.LOAD){
		i.value = this.getInput(i.name);
		//System.out.println("Load var i:"+i);
		stack.push(i);
	    }else if(i.instr == Instr.CONSTANT){
		stack.push(i);
	    }else{
		if(i.type.equals("binop")){
		    // FIXME: test stack-size == 2
		    System.out.println("binop with stacksize: "+stack.size());
		    Double val1 = stack.pop().value;
		    if(val1 == null){
			System.out.println("WARNING: 1st stack.pop.value is null");
			return null;
		    }
		    Double val2 = stack.pop().value;
		    if(val2 == null){
			System.out.println("WARNING: 2nd stack.pop.value is null");
			return null;
		    }
		    System.out.println("WARNING: binop with  val1: "+val1+" val2:"+ val2);
		    i.binexec(val1,val2);
		    stack.push(i);
		}else if(i.type.equals("aggrop")){
		    i.aggrexec(stack.pop().name,data);
		    stack.push(i);
		}
	    }
	}
	if(stack.size()==1){
	    Instruction last = stack.pop();
	    System.out.println("return value for last Instruction "+last);
	    return last.value;
	}else{
	    System.out.println("WARNING: Stack-size is "+stack.size());
	}
	return null;
    }

    



    public static void main( String args[] ) {
       if(args.length < 1){
	   System.out.println("Usage, see testing.txt");
       }else{
	   String opspec = args[0];
	   System.out.println("Gcalc, got op-spec: "+opspec);
	   
	   OperatorObj qops = new OperatorObj();
	   qops.init(opspec);

	   String input_data = args[1];
	   System.out.println("Gcalc, got input-data: "+input_data);
	   for(String x : input_data.split(",")){
	       String[] tup = x.split(":");
	       if(tup.length==2){
		   qops.putInput(tup[0],tup[1]);
	       }
	   }
	   qops.showInput();
	   qops.printIlist();
	   System.out.println("targetVar: "+qops.targetVar);
	   try{
	       Double ret = qops.execute();
	       System.out.println("ret: "+ret);
	   }catch(InstrException e){
	       System.out.println(e.toString());
	   }


       }
   }
    
}
