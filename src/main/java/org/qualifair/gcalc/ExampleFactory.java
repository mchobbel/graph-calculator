package org.qualifair.gcalc;

import java.util.List;
import org.apache.commons.configuration2.BaseConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.VertexProperty;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerFactory;


import static org.apache.tinkerpop.gremlin.structure.io.IoCore.gryo;


public final class ExampleFactory {
    int idcounter = 0;

    int nr_children=5;
    int nr_layers=4;
    
    private ExampleFactory() {}
    ExampleFactory(int layers,int children) {
	nr_children=children;
	nr_layers=layers;
    }    

    public  TinkerGraph createExample2() {
        final TinkerGraph g = getTinkerGraphWithNumberManager();
        generateExample2(g);
        return g;
    }
    public  void generateExample2(final TinkerGraph g) {
	   if(g==null){
	       System.out.println("Ooops TinkerGraph is null!");
	       return;
	   }
	   idcounter++;
	   final Vertex vertex_a0 = g.addVertex(T.id, idcounter, T.label, "AQS", "name", "vertex_a0");
	   vertex_a0.property("operator","zsum = SUM(z)");
	   add_vertices_recursively(g,vertex_a0,0);

    }
    public  void add_vertices_recursively(final TinkerGraph g, Vertex current,int layer) {

	if(layer==nr_layers){
	    return;
	}
	layer++;
	String prefix = "L"+layer;
	for(int i=0;i<nr_children;i++){
	    idcounter++;
	    String name = "vertex_"+prefix+"."+i;
	    final Vertex v = g.addVertex(T.id, idcounter, T.label, "AQS", "name", name);
	    v.property("layer",layer);
	    v.property("i",i);
	    if(layer > 3){
		double val = (0.1+idcounter);
		v.property("p",val);
	    }else if(layer==3){
		v.property("operator","x=DIV(SUM(p),c)");
	    }else if(layer==2){
		v.property("operator","y=DIV(SUM(x),c)");
	    }else if(layer==1){
		v.property("operator","z=DIV(SUM(y),c)");
	    }
	    v.addEdge("for",current);
	    add_vertices_recursively(g,v,layer);
	}
	idcounter++;
	final Vertex c = g.addVertex(T.id, idcounter, T.label, "AQS", "name", "count");
	c.property("c", (1.01*nr_children));
	c.addEdge("for",current);	

    }

   private  TinkerGraph getTinkerGraphWithNumberManager() {
        final Configuration conf = getNumberIdManagerConfiguration();
	if(conf==null){
	    System.out.println("huh conf is null?");
	}
	
	
        return TinkerGraph.open(conf);
    }

    private Configuration getNumberIdManagerConfiguration() {
        final Configuration conf = new BaseConfiguration();
        conf.setProperty(TinkerGraph.GREMLIN_TINKERGRAPH_VERTEX_ID_MANAGER, TinkerGraph.DefaultIdManager.INTEGER.name());
        conf.setProperty(TinkerGraph.GREMLIN_TINKERGRAPH_EDGE_ID_MANAGER, TinkerGraph.DefaultIdManager.INTEGER.name());
        conf.setProperty(TinkerGraph.GREMLIN_TINKERGRAPH_VERTEX_PROPERTY_ID_MANAGER, TinkerGraph.DefaultIdManager.LONG.name());
        return conf;
    }

}

