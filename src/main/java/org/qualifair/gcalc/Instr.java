package org.qualifair.gcalc;

public enum Instr {
    SUM,PROD,
    PLUS,MIN,DIV,TIMES,
    CONSTANT,LOAD;

    boolean isbinex(){
	return (
		this == PLUS||
		this == MIN||
		this == DIV ||
		this == TIMES
		);
    }
    boolean isaggr(){
	return (
		this == SUM ||
		this == PROD
		);
    }
}
