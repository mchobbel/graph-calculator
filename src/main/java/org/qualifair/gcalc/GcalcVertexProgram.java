package org.qualifair.gcalc;

import org.apache.commons.configuration2.Configuration;
import org.apache.tinkerpop.gremlin.process.computer.GraphComputer;
import org.apache.tinkerpop.gremlin.process.computer.Memory;
import org.apache.tinkerpop.gremlin.process.computer.MemoryComputeKey;
import org.apache.tinkerpop.gremlin.process.computer.MessageCombiner;
import org.apache.tinkerpop.gremlin.process.computer.MessageScope;
import org.apache.tinkerpop.gremlin.process.computer.Messenger;
import org.apache.tinkerpop.gremlin.process.computer.VertexComputeKey;
import org.apache.tinkerpop.gremlin.process.computer.VertexProgram;
import org.apache.tinkerpop.gremlin.process.computer.util.AbstractVertexProgramBuilder;
import org.apache.tinkerpop.gremlin.process.traversal.Operator;
import org.apache.tinkerpop.gremlin.process.traversal.Traversal;
import org.apache.tinkerpop.gremlin.process.traversal.TraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.process.traversal.util.PureTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.util.ScriptTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.util.TraversalUtil;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Direction;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.VertexProperty;
import org.apache.tinkerpop.gremlin.structure.util.StringFactory;
import org.apache.tinkerpop.gremlin.util.iterator.IteratorUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.javatuples.Triplet;

import java.net.URLClassLoader;
import java.net.URL;


/**
 * @author Marko A. Rodriguez (http://markorodriguez.com)
 */


public class GcalcVertexProgram implements VertexProgram<Triplet<String,String,Double>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GcalcVertexProgram.class);

    //public static final String PAGE_RANK = "PRank";
    
    private static final String EDGE_COUNT = "gremlin.pageRankVertexProgram.edgeCount";
    //private static final String PROPERTY = "gremlin.pageRankVertexProgram.property";
    private static final String VERTEX_COUNT = "gremlin.pageRankVertexProgram.vertexCount";

    private static final String INPUTS = "gcalc.GcalcVertexProgram.inputs";

    private static final String MAX_ITERATIONS = "gremlin.pageRankVertexProgram.maxIterations";
    private static final String EDGE_TRAVERSAL = "gremlin.pageRankVertexProgram.edgeTraversal";
    private static final String INITIAL_RANK_TRAVERSAL = "gremlin.pageRankVertexProgram.initialRankTraversal";

    private static final String CONVERGENCE_ERROR = "gremlin.pageRankVertexProgram.convergenceError";

    private MessageScope.Local<Triplet<String,String,Double>> incidentMessageScope = MessageScope.Local.of(__::outE);
    private MessageScope.Local<Triplet<String,String,Double>> countMessageScope = MessageScope.Local.of(new MessageScope.Local.ReverseTraversalSupplier(this.incidentMessageScope));
    private PureTraversal<Vertex, Edge> edgeTraversal = null;
    private PureTraversal<Vertex, ? extends Number> initialRankTraversal = null;

    private String inputs = "";
    private double epsilon = 0.00001d;
    private int maxIterations = 0;
    //private String property = PAGE_RANK;
    private String targetVar = "target-var";
    private String basedOnInputCount = "based-on-input-count";
    private String operatorResult = "operator-result";
    
    private Set<VertexComputeKey> vertexComputeKeys;
    private Set<MemoryComputeKey> memoryComputeKeys;

    private GcalcVertexProgram() {

    }

    List<Triplet<String,String,Double>> getDlist(Iterator<Triplet<String,String,Double>> it){
	List<Triplet<String,String,Double>> ret = new ArrayList<Triplet<String,String,Double>>();
	while(it.hasNext()){
	    ret.add(it.next());
	}
	return ret;
    }
	
    @Override
    public void loadState(final Graph graph, final Configuration configuration) {
	logg("loadState");
        if (configuration.containsKey(INITIAL_RANK_TRAVERSAL))
            this.initialRankTraversal = PureTraversal.loadState(configuration, INITIAL_RANK_TRAVERSAL, graph);
        if (configuration.containsKey(EDGE_TRAVERSAL)) {
            this.edgeTraversal = PureTraversal.loadState(configuration, EDGE_TRAVERSAL, graph);
            this.incidentMessageScope = MessageScope.Local.of(() -> this.edgeTraversal.get().clone());
            this.countMessageScope = MessageScope.Local.of(new MessageScope.Local.ReverseTraversalSupplier(this.incidentMessageScope));
        }

	this.inputs = configuration.getString(INPUTS,this.inputs);

        this.maxIterations = configuration.getInt(MAX_ITERATIONS, 20);
        //this.property = configuration.getString(PROPERTY, PAGE_RANK);
        this.vertexComputeKeys = new HashSet<>(Arrays.asList(
	        VertexComputeKey.of(targetVar, false),
		VertexComputeKey.of(basedOnInputCount, false),
		VertexComputeKey.of(operatorResult, false),
                VertexComputeKey.of(EDGE_COUNT, true)));
        this.memoryComputeKeys = new HashSet<>(Arrays.asList(

                MemoryComputeKey.of(VERTEX_COUNT, Operator.sum, true, true),
                MemoryComputeKey.of(CONVERGENCE_ERROR, Operator.sum, false, true)));
    }

    @Override
    public void storeState(final Configuration configuration) {
	logg("storestate");
        VertexProgram.super.storeState(configuration);

	configuration.setProperty(INPUTS, this.inputs);
	
        //configuration.setProperty(PROPERTY, this.property);
        configuration.setProperty(MAX_ITERATIONS, this.maxIterations);
        if (null != this.edgeTraversal)
            this.edgeTraversal.storeState(configuration, EDGE_TRAVERSAL);
        if (null != this.initialRankTraversal)
            this.initialRankTraversal.storeState(configuration, INITIAL_RANK_TRAVERSAL);
    }

    @Override
    public GraphComputer.ResultGraph getPreferredResultGraph() {
        return GraphComputer.ResultGraph.NEW;
    }

    @Override
    public GraphComputer.Persist getPreferredPersist() {
        return GraphComputer.Persist.VERTEX_PROPERTIES;
    }

    @Override
    public Set<VertexComputeKey> getVertexComputeKeys() {
        return this.vertexComputeKeys;
    }


    @Override
    public Set<MemoryComputeKey> getMemoryComputeKeys() {
        return this.memoryComputeKeys;
    }

    @Override
    public Set<MessageScope> getMessageScopes(final Memory memory) {
        final Set<MessageScope> set = new HashSet<>();
        set.add(memory.isInitialIteration() ? this.countMessageScope : this.incidentMessageScope);
        return set;
    }

    @Override
    public GcalcVertexProgram clone() {
        try {
            final GcalcVertexProgram clone = (GcalcVertexProgram) super.clone();
            if (null != this.initialRankTraversal)
                clone.initialRankTraversal = this.initialRankTraversal.clone();
            return clone;
        } catch (final CloneNotSupportedException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public void setup(final Memory memory) {
	logg("setup");

        memory.set(VERTEX_COUNT, 0.0d);
        memory.set(CONVERGENCE_ERROR, 1.0d);
    }
    void logg(String msg){
	System.out.println("Gcalc: "+msg);
    }
    Double readVertexDouble(Vertex vertex,String propname){
	return vertex.<Double>property(propname).orElse(null);
	/*
	VertexProperty p =  vertex.property(propname);
	if(p!=null && p.isPresent()){
	    return (Double)p.value();
	}
	return null;
	*/
	
    }
    String readTargetVar(Vertex vertex){
	return vertex.<String>property(targetVar).orElse(null);
	/*
	VertexProperty p =  vertex.property(targetVar);
	if(p!=null && p.isPresent()){
	    return (String)p.value();
	}
	return null;
	*/
    }
    boolean has_fresh_input(List<Triplet<String,String,Double>> inbox){
	for(Triplet<String,String,Double> p: inbox){
	    if("fresh".equals(p.getValue1())){
		return true;
	    }
	}
	return false;
    }
    void debugEdges(Vertex v){
	Iterator<Edge> edges = v.edges(Direction.OUT);
	while(edges.hasNext()){
	    Edge edg = edges.next();
	    logg(" - outE() "+edg);
	    Vertex v2 = edg.inVertex();
	    try{
		logg("  - - inV() "+v2.label()+ " "+v2.property("name"));
	    }catch(UnsupportedOperationException ex){
		logg("  - - Oops, label cannot be read");
	    }
	}
    }
    Double run_operator(Vertex vertex, OperatorObj operator, List<Triplet<String,String,Double>> inbox){
	//operator.printIlist();
	int count = inbox.size();

	logg("run_operator with "+count+" input messages");
	if(count==0){
	    logg("WARNING: no messages in run_operator()");
	    return null;
	}
	if(operator==null){
	    logg("WARNING: operator is null in run_operator()");
	    return null;
	}

	for(Triplet<String,String,Double> message: inbox){
	    operator.putInputD(message.getValue0(),message.getValue2());
	}
	try{
	    Double result = operator.execute();
	    logg("operatorResult : "+result);
	    if(result!=null){
		logg("store operatorResult ");
		vertex.property(VertexProperty.Cardinality.single, operatorResult, result);
		logg("store inbox.size as basedOnInputCount");
		vertex.property(VertexProperty.Cardinality.single, basedOnInputCount,count);
		return result;

	    }
	}catch(InstrException e){
	    logg(e.toString());
	}
	return null;
    }
    @Override
    public void execute(final Vertex vertex, Messenger<Triplet<String,String,Double>> messenger, final Memory memory) {
	String name = (String) vertex.property("name").value();

	String op_str = vertex.<String>property("operator").orElse("");
	String v_str = " Vertex " + vertex.id() + " "+vertex.label()+" "+name + " (op:"+op_str + ")";
	OperatorObj operator = new OperatorObj();


	
        if (memory.isInitialIteration()) {
	    logg("execute Initial "+v_str);
            //messenger.sendMessage(this.countMessageScope, 1.0d);
            memory.add(VERTEX_COUNT, 1.0d);
        } else {
	    logg("execute Iteration == "+ memory.getIteration() + v_str);
	    Double result = null;
	    if(!op_str.equals("")){
		String message_status = "cold";
		String trg_var = readTargetVar(vertex);
		if(trg_var ==null){
		    logg("operator.init (1)"+op_str);
		    operator.init(op_str);

		    logg("trg-var is null. Store it now (we have "+operator.targetVar+" in opspec)");	
		    // store target-var-name as vertex-property
		    vertex.property(VertexProperty.Cardinality.single, this.targetVar, operator.targetVar);
		}else{
		    logg("trg-var is : "+trg_var);
		}
		debugEdges(vertex);
		logg("check stored-result");
		result = readVertexDouble(vertex,operatorResult);
		List<Triplet<String,String,Double>> inbox = getDlist( messenger.receiveMessages());
		double converge=0.0d;
		if(result==null){
		    logg("operator.init (2)"+op_str);
		    operator.init(op_str);
		    result = run_operator(vertex,operator,inbox);
		    message_status = "fresh";
		    converge = 1.0d;
		}else{
		    logg("found stored-result: "+result);
		    Integer based_on_input_count = vertex.<Integer>property(basedOnInputCount).orElse(0);

		    if(inbox.size() > based_on_input_count){
			logg("input-count changed. Need to run it again!");
			operator.init(op_str);
			result = run_operator(vertex,operator,inbox);
			message_status = "fresh";
			converge = 1.0d;
		    }else if(has_fresh_input(inbox)){
			logg("inbox contains fresh-input(s). Need to run it again!");
			operator.init(op_str);
			result = run_operator(vertex,operator,inbox);
			message_status = "fresh";

			converge = 1.0d;
		    }
		}
		memory.add(CONVERGENCE_ERROR, converge);
		

		if(trg_var!=null && result !=null){
		    logg("send result-message : "+trg_var+"="+result);
		    messenger.sendMessage(this.incidentMessageScope, new Triplet(trg_var,message_status,result));
		}
	    } // op_str != ""

            //////////////////////////
	    logg("sending primary inputs: "+this.inputs);
	    String[] props = this.inputs.split(",");
	    for(String prop : props){
		Double dval = vertex.<Double>property(prop).orElse(null);
		if (dval != null){
		    logg("send prim-input-message "+prop+"="+dval);
		    messenger.sendMessage(this.incidentMessageScope, new Triplet(prop,"primary",dval));
		}
	    }
        }
    }

    @Override
    public boolean terminate(final Memory memory) {
	double conv_err = memory.<Double>get(CONVERGENCE_ERROR);
	logg("convergence-error "+conv_err);

        boolean terminate = conv_err < this.epsilon || memory.getIteration() >= this.maxIterations;

        memory.set(CONVERGENCE_ERROR, 0.0d);
        return terminate;

    }

    @Override
    public String toString() {
        return StringFactory.vertexProgramString(this,  " epsilon=" + this.epsilon
						 + ", inputs=" + this.inputs
						 + ", iterations=" + this.maxIterations);
    }

    //////////////////////////////


    public static Builder build() {
	//Builder.printLoadedClasses();
        return new Builder();
    }

    public final static class Builder extends AbstractVertexProgramBuilder<Builder> {
	public static void printLoadedClasses(){
	    URL[] classes= ((URLClassLoader) (Thread.currentThread().getContextClassLoader())).getURLs();
	    for(int i=0;i<classes.length;i++){
		URL x = classes[i];
		System.out.println(x);
	    }
	}

        private Builder() {
            super(GcalcVertexProgram.class);
        }
        public Builder inputs(final String inputs) {
            this.configuration.setProperty(INPUTS,inputs);
	    //this.configure(INPUTS,inputs);
            return this;
        }
	

    }
    @Override
    public Features getFeatures() {
        return new Features() {
            @Override
            public boolean requiresLocalMessageScopes() {
                return true;
            }

            @Override
            public boolean requiresVertexPropertyAddition() {
                return true;
            }
        };
    }
}
