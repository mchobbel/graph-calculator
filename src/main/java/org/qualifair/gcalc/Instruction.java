package org.qualifair.gcalc;

import java.util.List;
import org.javatuples.*;

class InstrException extends Exception{
    InstrException(String m){
	super(m);
    }
};

class Instruction{
    String name = "";
    Instr instr = null;
    String type = "";
    Double value = null;
	
    
    Instruction(String n,String t){
	if(t.equals("binop") || t.equals("aggrop")){
	    this.instr =  Instr.valueOf(n);
	}else if(t.equals("constant")){
	    this.instr = Instr.CONSTANT;
	}else{
	    this.instr = Instr.LOAD;
	    this.name = n;
	}
	this.type = t;
    }
    public void aggrexec(String key, List<Pair<String,Double>> list)  throws InstrException{
	if(!instr.isaggr()){
	    throw new InstrException("not an aggr instruction: "+instr.toString());
	}
	//System.out.println("Gcalc aggr-exec with key:"+key);
	Double ret = 0.0;
	for(Pair<String,Double> p:list){
	    if(p.getValue0().equals(key)){
		if(instr == Instr.SUM)
		    ret += p.getValue1();
	    }
	}
	value = ret;
	System.out.println("Instruction.aggrexec: "+toString());
    }
    public void binexec(Double v1, Double v2) throws InstrException{
	if(!instr.isbinex()){
	    throw new InstrException("not a binex instruction: "+instr.toString());
	}
	if(v1==null || v2==null) return;
	if(instr == Instr.PLUS)
	    value = v1+v2;
	else if(instr == Instr.MIN)
	    value = v1-v2;
	else if(instr == Instr.DIV)
	    value = v1/v2;
	else if(instr == Instr.TIMES)
	    value = v1*v2;
	else
	    value = null;
	System.out.println("Instruction.binexec: "+toString()+" with v1:"+v1+" , v2:"+v2);
    }
    public String toString(){
	return "Instruction :"+instr+" (" + name + ") result:"+value;
    }
}
