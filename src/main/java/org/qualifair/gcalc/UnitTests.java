package org.qualifair.gcalc;

public class UnitTests {
    int count=0;
    int good=0;
	
    public void run(String opspec, String inputs, double expect){
	count+=1;
	Double ret = exe(opspec,inputs);
	if(ret==null){
	    System.out.println("ERROR ret is null");
	    return;
	}

	double diff = Math.abs(ret-expect);

	if(diff> 0.001d){

	    System.out.println("ERROR: diff: "+diff);
	    System.out.println("   for opspec: "+opspec);
	    System.out.println("   expected: "+expect);
	    System.out.println("   but result was: "+ret);
	    
	}else{
	    good+=1;
	}
    }
    public Double exe(String opspec, String inputs){
	OperatorObj qops = new OperatorObj();
	qops.init(opspec);
	
	for(String x : inputs.split(",")){
	    String[] tup = x.split(":");
	    //System.out.println("  got input x:"+x);
	    if(tup.length==2){
		qops.putInput(tup[0],tup[1]);
	   }
	}
	/*
	  System.out.println("inputs:");
	  qops.showInput();
	  System.out.println("instructions:");
	  qops.printIlist();
	*/
	try{
	    return qops.execute();
	}catch(InstrException e){
	    e.printStackTrace();
	}
	return null;

    }
    public void runall(){
	run("z = PLUS(x,y)" , "x:1.1,y:2.2", 3.3);
	run("z = PLUS(x,PLUS(INT(3),y))" , "x:1.1,y:2.2", 6.3);
	run("z = DIV(x,INT(3))" , "x:6.6", 2.2);
	run("z = DIV(SUM(x),INT(3))" , "x:6.6,x:3.3", 3.3);
	run("z = DIV(TIMES(x,y),INT(3))" , "x:6.6,y:4", 8.8);
	run("z = PLUS(FLOAT(3.1),FLOAT(1.3))","",4.4);

    }
    public void report(){
	System.out.println("Finished running "+good+" successfull out of "+count+" tests");
	if(good<count){
	    System.out.println("FATAL : not all good!" );
	}else{
	    System.out.println("All good!");
	}
    }
    public static void main(String[]args){
	UnitTests tests = new UnitTests();
	tests.runall();
	tests.report();
    }
}
