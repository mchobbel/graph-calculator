
Create big jar for spark-job:
 # cd cql-shaded-package
 # mvn package > stdout.log
 # cp target/janusgraph-spark-0.0.1-SNAPSHOT.jar /var/gcalc/

Add it to spark-job properties like:

  spark.executor.extraClassPath=/var/gcalc/janusgraph-spark-0.0.1-SNAPSHOT.jar

