#!/bin/bash


ANTLR=antlr-4.7.1-complete.jar
JARFN=gcalc-parser.jar
export CLASSPATH=.:./$ANTLR:./$JARFN

PACKAGE=org.qualifair.parser
PDIR=$(echo $PACKAGE | sed -s 's,\.,/,g')
MAINCL=$PACKAGE".Gcalc"

JAVAC=$JAVA_SDK/bin/javac


if [ $# -lt 1 ]
then
    echo "Usage: $0 down |  gen | comp |install |test| try (-gui|-tree) <input) | clean | ver"
    echo " down : download $ANTLR"
    echo " gen  : generate .java files"
    echo " comp : compile .java files + make jar"
    echo " install : install jar in ../repo"
    echo " try -gui <INPUT> : run input using GUI-mode"
    echo " try -tree <INPUT> : run input using Tree-mode"    
    echo " test : parse example input (good + bad)"
    echo " ver : show javac version"
    exit
fi

cmd=$1
shift

if [ $cmd == "down" ]
then
   curl -O https://www.antlr.org/download/$ANTLR
fi

if [ ! -f $ANTLR ]
then
    echo "The vendor-jar file  $ANTLR is missing"
    echo " use $0 down"
    exit
fi



if [ $cmd == "gen" ]
then
    java org.antlr.v4.Tool -package $PACKAGE -o out/$PDIR Gcalc.g4
    echo "generate java files in out/$PDIR"
fi

if [ $cmd == "comp" ]
then
    $JAVAC -sourcepath out out/$PDIR/Gcalc*java
    jar cvf $JARFN -C out/ .
fi

if [ $cmd == "try" ]
then
    if [ $# -lt 2 ]
    then
	echo " $0 try -gui | -tree <INPUT> "
	echo " e.g. $0 try -gui \"PLUS(SUM(x),yyy)\" "
	exit
    fi
    if [ $1 == "-gui" ] || [ $1 == "-tree" ]
    then
	echo $2 | java org.antlr.v4.gui.TestRig $MAINCL r $1
    else
	echo "you said $1 instread of -gui of -tree"
	exit
    fi
    

fi

if [ $cmd == "test" ]
then
    inf=test/goodinput.txt
    inf2=testbadinput.txt
    echo "Running parser on Good input $inf"
    java org.antlr.v4.gui.TestRig $MAINCL r $inf
    echo "no error so far?"
    sleep 2
    echo "Running parser on  $inf2"
    for x in test/bad*.txt
    do
	echo $x
	java org.antlr.v4.gui.TestRig $MAINCL r $x
    done
fi

if [ $cmd == "clean" ]
then
    echo "Remove all Gcalc.*"
    #rm GcalcQfsOp*.class Qfs*.interp Qfs*.java Qfs*.tokens
fi

if [ $cmd == "install" ]
then
    echo "Install jar"
    trgdir="../repo/qware/gcalc-parser/0.0.1/"
    mkdir -p $trgdir
    echo "copy $JARFN to $trgdir"
    cp $JARFN  $trgdir"/gcalc-parser-0.0.1.jar"
    echo "perhaps you need to run mvn with -U switch :"
    echo 'cd ..'
    echo "  mvn -U compile"
fi

if [ $cmd == "ver" ]
then
    $JAVAC -version
fi




   


