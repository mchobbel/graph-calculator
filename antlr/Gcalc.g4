grammar Gcalc;

r :
  var '='  (binexp | singularexp | aggrexp)   ;
binexp: binop '(' exp ',' exp ')';
singularexp: singop '(' exp ')';
aggrexp: aggrop '(' var  ')';
constant: 'INT(' integer ')' | 'FLOAT(' floating ')';
binop: 'PLUS'
  | 'MIN'
  | 'DIV'
  | 'TIMES'
  ;
aggrop:  'SUM'
  | 'PROD'
  ;
singop: 'ABS'
  | 'ROUND'
  | 'SQRT'
  ; 


exp: var
  | binexp
  | singularexp
  | aggrexp
  | constant
  ;


integer: INTVAL;
INTVAL :[0-9]+;
floating: FLOATVAL;
FLOATVAL : [0-9]+ '.' [0-9]+;

var: VARNAME;
VARNAME :[a-z]+ [0-9]* ;

WS : [ \t\r\n]+ -> skip ;

