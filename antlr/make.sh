#!/bin/bash

if [ ! -f antlr-4.7.1-complete.jar ]
then
    ./run.sh down
fi

./run.sh gen
./run.sh comp
./run.sh install

./run.sh ver
