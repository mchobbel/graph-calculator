#!/bin/bash

export CLASSPATH=.:./antlr-4.7.1-complete.jar:./out

if [ $# -lt 1 ]
then
    echo "Usage: $0   g4 | gen | comp | gui | clean"
    echo " g4   : create the Hello.g4 file"
    echo " gen  : generate .java files"
    echo " comp : compile .java files"
    echo " gui  : run GUI"
    exit
fi

cmd=$1
shift

if [ $cmd == "g4" ]
then
   ( cat<<EOF

grammar Hello;
r  : 'hello' ID ;
ID : [a-z]+ ;
WS : [ \t\r\n]+ -> skip ;

EOF
   ) > Hello.g4

fi

if [ $cmd == "gen" ]
then
    java  org.antlr.v4.Tool -o out Hello.g4
fi

if [ $cmd == "comp" ]
then
    javac -sourcepath out out/Hello*.java
fi

if [ $cmd == "gui" ]
then
    echo "Try : hello joejoe (followed by enter + CTRL-D)"
    java org.antlr.v4.gui.TestRig Hello r -gui
fi

if [ $cmd == "clean" ]
then
    echo "Remove all Hello.*"
    rm Hello*
fi




   


