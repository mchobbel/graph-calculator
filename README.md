# Graph calculator

Graph calculator is a Gremlin VertexProgram for doing  complex arithmetic calculations using a graph. The graph is the structure that initially contains the
input data plus the algorithm and, after execution contains the resulting data.

### Technologies

This software depends on the following technologies:

 * [VertexProgram](https://tinkerpop.apache.org/docs/3.3.3/reference/#vertexprogram) : Standard Gremlin component, which developers can use to build distrubuted algorithms.
 * [Janusgraph](https://janusgraph.org/) : Graph-database including Gremlin/tinkerpop + plugins

Furthermore, to run the examples, you need:

 * [Hadoop](http://hadoop.apache.org/) : Distribubuted computing platform
 * [Spark](https://spark.apache.org/) : Analysis engine for large-scale data

 See [doc/install](doc/install.md) for instructions howto install + configure this software.

### Introduction

Before running the Graph Calculator you need to have an initial graph
which contains:

 * Vertices:
    * __Input-vertices__ contain input numbers as Vertex-properties (e.g. a=2.1 b=3.2)
    * __Operator-vertices__ contain operator-specs, which are arithmetic functions (eg. "a + b")
 * Edges to connect each Operator-vertex to its "inputs" so the arithmetic
 function can be applied to those input numbers.

    
Whenever an Operator vertex is executed

 * it yields a numeric result
 * which can serve as input to other Operator-vertices.


Each Operator vertex can start execution only if the inputs are
present. The primary operators are the ones that have their input
already present in the initial state of the graph so they are the
first to execute. The secundary operators are dependent upon results
provided by other operators so they have to wait for their inputs to
become available.

### Operator-specs
Each Operator-vertex contains an Operator-spec. This is a string vertex-property, which contains a statement like this:

```
  <RESULT-VAR> = <FORMULA>
```


 * RESULT-VAR : the property-name which will store the resulting number, e.g. ```result1```.
 * FORMULA : the arithmetic operation, e.g. ```PLUS(a,b)```



So, for example, the operator-spec ```  result1 = PLUS(a,b) ```
tells the program to find input-values of _a_ and _b_, add them and store the
resulting value as _result1_.

The formula can be a nested expression. The following expression
tells the program to take inputs a,b and c and then to calculate ```a+(b*c)```
and finally to store the resulting value as _result2_.

```
  result2 = PLUS(a, TIMES(b,c))
```


#### Operators

The different operator types are:

 * __singular__ : taking 1 input 
 * __binary__ : taking 2 inputs, must have both inputs present otherwise the result is _null_ (i.e. faulty)
 * __agragate__ : find all values of a given property-name and perform n-ary function.
 * __constant__ : a fixed input number which is given in the formula directly.


All different operators are

| syntax     | math form | Type  | Remarks
| ---------- | ----------|-------|-------
| PLUS(x,y)  |  x + y    | binary |
| MIN(x,y)   |  x - y    | binary |
| DIV(x,y)   |  x / y    | binary |
| TIMES(x,y) |  x * y    | binary |
| SUM(x)     |  ∑x    | aggregate | yields 0 when inputs absent 
| PROD(x)    | ∏x     | aggregate | yields 1 when inputs absent
| ABS(x)     | \|x\|    | singular |
| FLOOR(x)   | ⌊ x ⌋    | singular |  nearest integer smaller than x
| SQRT(x)    |  √x  | singular
| SQR(x)     |  x²  | singular
| FLOAT(n.n) |    | constant
| INT(n)     |    | constant


### An example graph

See [doc/examples](doc/examples.md) for instructions how to run
this example.

#### Input-graph

![example-1a](doc/example-1a.png)

See figure above. The blue text (e.g. operator) are keywords (reserved names) for the graph-calculator. Other text, ( e.g. sum1) are variable names.

The input-graph below is  composed of 6 vertices.


- Vertices a1,a2 and a3 contain input-numbers 3.1, 3.2, and 4.3
    - all assigned to a variable named _x_
- Vertex b2 contains input number 3, assigned to variable _y_
- Vertex b1 contains  `operator: sum1=SUM(x)`
    - the keyword `operator` declares an operator-spec
    - SUM(_x_) 
        - Find all input-vertices of b1 (these are b1-connected-vertices with the arrow pointing to b1) : a1,a2 and a3
		- read all their x-values and summarize them.
	- _sum1_ is the variable to contain the operator's output value.
- Vertex c1 contains `operator: myresult=DIV(sum1,y)`
	- DIV(_sum1_,_y_)
	    - Find all input-vertices of C1, only 2 are expected
		- Read the variable _sum1_
		- Read the variable _y_	(this is the value 3.0)
	-  _myresult_ is the variable to contain the output.



![example-1b](doc/example-1b.png)


See figure above. 


This graph is calculated in 2 iterations:

 * The first iteration,  in Vertex b1
    * All values of _x_ are summarized (3.1 + 3.2 + 4.3)
    * the resulting value 10.6 is stored as _sum1_.
 * The 2nd iteration, in Vertex c1
    * Value _sum1_ (from Vertex b1) and y (from b2) are taken as input values
    * The result is calculated as: _sum1_ / _y_ = 10.6 / 3 = 3.53
    * and stored in the graph as _myresult_
	

