
// use :load <this> from the console

g.addV('AQS').as('b0').property('name','b0').property('op','s=SUM(x)').
  addV('AQS').as('a1').property('name','a1').property('x',3.1d).
  addE('for').from('a1').to('b0').
  addV('AQS').as('a2').property('name','a2').property('x',3.2d).
  addE('for').from('a2').to('b0').
  addV('AQS').as('a3').property('name','a3').property('x',4.3d).
  addE('for').from('a3').to('b0')

  
println("created Vertices b0,a1,a2,a3")
g.V().count()
g.addV('AQS').as('c0').property('name','c0').property('op','z=DIV(s,y)').
  addV('AQS').as('b1').property('name','b1').property('y',3.0d).
  addE('for').from('b1').to('c0')

println("created Vertices b1,c0")

g.V().has('name','b0').as('b0').
  V().has('name','c0').as('c0').
  addE('for').from('b0').to('c0')

println("created Edge b0 > c0")
