def getConfiguration(){
    props = new java.util.Properties()
    props.put('gremlin.graph', 'org.apache.tinkerpop.gremlin.hadoop.structure.HadoopGraph')
    props.put('gremlin.hadoop.graphReader','org.apache.tinkerpop.gremlin.hadoop.structure.io.graphson.GraphSONInputFormat')
    props.put('gremlin.hadoop.inputLocation','data/example1-v3.gson')
    return new MapConfiguration(props)
}


// now you can:
//
conf = getConfiguration()
graph = GraphFactory.open(conf)



