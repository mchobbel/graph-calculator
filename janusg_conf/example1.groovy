
graph = JanusGraphFactory.open('conf/janusgraph-inmemory.properties')
g = graph.traversal()


:load janusg_conf/example1_data.groovy


gio = GraphSONIo.build(GraphSONVersion.V1_0)
fn = 'example1.gson'
graph.io(gio).writeGraph(fn)

println("wrote to "+fn)

