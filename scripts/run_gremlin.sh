#!/bin/bash

CFG=scripts/_env.sh
if [ ! -f $CFG ] ; then
    echo "ERROR: $CFG is not found here"
    exit
fi

. $CFG


cd $JANUS_HOME



if [ -z $GCALC_HOME ] ; then
    echo "need environment var GCALC_HOME to be set"
    exit
fi
   
if [ -z $HADOOP_HOME ] ; then
    echo "need environment var HADOOP_HOME to be set"
    exit
fi
# preflight test:
if [ ! -f lib/janusgraph-core-$JANUS_VER.jar ]
then
    echo "Please cd into the janusgraph-$JANUS_VER distro"
    exit
fi


if [ ! -h janusg_conf ]
then
    echo "need symlink janusg_conf to be present"
    echo " ln -s $GCALC_HOME/janusg_conf"
    ln -s  $GCALC_HOME/janusg_conf

fi

if [ $(jps|grep -c Master) -eq 0 ] ; then
    echo "JPS (spark) Master not found "
    exit
fi
if [ $(jps|grep -c Worker) -eq 0 ] ; then
    echo "JPS (spark) Worker not found"
    exit
fi
if [ $(jps|grep -c NameNode) -eq 0 ] ; then
    echo "Hadoop NameNode not found"
    exit
fi



export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export CLASSPATH=$HADOOP_CONF_DIR


if [ "x"$1 == "x-skip" ] ; then
    shift
    bin/gremlin.sh   $*
elif [ "x"$1 == "x-unitt" ] ; then
    bin/gremlin.sh -i janusg_conf/unit_test_base.gr -i janusg_conf/unit_test1.gr 
else
    bin/gremlin.sh  -i janusg_conf/all.gr $*    
fi


