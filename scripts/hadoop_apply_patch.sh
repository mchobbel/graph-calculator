#!/bin/bash

if [ -z $HADOOP_HOME ] ; then
    echo "ERROR"    
    echo "need environment var HADOOP_HOME to be set"
    exit
fi
if [ -z $GCALC_HOME ] ; then
    echo "ERROR"
    echo "need environment var GCALC_HOME to be set"
    exit
fi

cd $HADOOP_HOME
patch -p1 < $GCALC_HOME/scripts/hadoop_config.patch 
