export GCALC_HOME=$HOME/git/gcalc
export VENDOR_PKGS=/mnt/data5/gcalc_pkgs

export JAVA_HOME=/etc/alternatives/jre_1.8.0
export JAVA_SDK=/etc/alternatives/java_sdk_1.8.0


export HADOOP_HOME=$VENDOR_PKGS/hadoop-2.10.2
export SPARK_HOME=$VENDOR_PKGS/spark-3.2.1-bin-hadoop2.7
export JANUS_VER=1.0.0-rc1
export JANUS_HOME=$VENDOR_PKGS/janusgraph-$JANUS_VER
export PATH=$PATH:$GCALC_HOME/scripts:$HADOOP_HOME/bin


