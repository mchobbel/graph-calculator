#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage basic| all"
    echo " cleanup objects / jars etc"
    echo " basic : only recompile"
    echo " all   : cleanup all (maven-repo, vendor-packages)"
    exit
fi


clean(){
    echo "clean $1"
    rm -rf $1
}
echo "Got 1: $1"
if [ $1 == "basic" ] || [ $1 == "all" ]
then
    clean ~/.m2/repository/qware/gcalc-parser
    clean repo
    clean target/
    clean antlr/out
    clean  antlr/gcalc-parser.jar
    clean  $HOME/.m2/repository/qware/parser
else
    echo "nothing"
fi



if [ $1 == "all" ]
then
    clean antlr/*.jar
fi


