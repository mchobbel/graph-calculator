#!/bin/bash


if [ -z $JANUS_HOME ] ; then
    echo "need environment var JANUS_HOME to be set"
    exit
fi

if [ ! -d /var/gcalc ] ; then
    echo "Please create /var/gcalc writable for $USER"
    exit
fi



unset JAVA_HOME
mvn -U compile
mvn package


for j in target/gcalc-0.0.1.jar \
	 repo/qware/gcalc-parser/0.0.1/gcalc-parser-0.0.1.jar \
	 antlr/antlr-4.7.1-complete.jar \
	 ~/.m2/repository/org/apache/commons/commons-configuration2/2.4/commons-configuration2-2.4.jar
do
    echo  $j
    if [ -f $j ]
    then
	cp $j $JANUS_HOME/lib/
	cp $j /var/gcalc/
    else
	echo " !!"
	echo " !! ERROR. file not found : $j"
	echo " !!"
    fi
    
done

if [ -f  $JANUS_HOME/lib/commons-configuration2-2.7.jar ]; then
    rm $JANUS_HOME/lib/commons-configuration2-2.7.jar

fi

