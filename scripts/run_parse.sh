#!/bin/bash

if [ -z $GCALC_HOME ] ; then
    echo "need environment var GCALC_HOME to be set"
    exit
fi

run(){
    verbosity=$1
    mvn exec:java -Dorg.slf4j.simpleLogger.defaultLogLevel=$verbosity \
	-Dexec.mainClass="org.qualifair.gcalc.OperatorObj" -Dexec.args="$2"
}
runt(){
    verbosity=$1
    mvn exec:java -Dorg.slf4j.simpleLogger.defaultLogLevel=$verbosity \
	-Dexec.mainClass="org.qualifair.gcalc.UnitTests" -Dexec.args="$2"
}


if [ $# -lt 1 ]
then
    echo "Usage: $0 [-v] | utests | exe <spec> "
    echo ' utest: do some unit-tests'
    echo ' exe <spec> execute the specified operation'
    echo '  spec: op-spec " " input-spec'
    echo '  - op-spec: e.g PLUS(x,y)'
    echo '  - input-spec: e.g.  x:3.3,y:3.4'
    echo " e.g. $0 exe " '"z=PLUS(x,y) x:3.3,y:3.4"'
    echo
    echo " -v : verbose"
    exit
fi



cd $GCALC_HOME

verbosity=ERROR
if [ $1 == "-v" ]
then
    verbosity=DEBUG
    shift
fi


if [ $1 == "exe" ]
then
    run $verbosity "$2"
fi

if [ $1 == "utests" ]
then
    runt
fi


