#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage $0 format | start | init"
    exit
fi

if [ -z $HADOOP_HOME ] ; then
    echo "need environment var HADOOP_HOME to be set"
    exit
fi


if [ $1 == "format" ]
then
   hdfs namenode -format
fi

if [ $1 == "start" ]
then
    $HADOOP_HOME/sbin/start-dfs.sh
fi

if [ $1 == "init" ]
then
    echo  hdfs dfs -mkdir -p /user/$USER/data
    hdfs dfs -mkdir -p /user/$USER/data
fi

