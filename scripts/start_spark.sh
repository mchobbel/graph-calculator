#!/bin/bash


if [ -z $SPARK_HOME ] ; then
    echo "need environment var SPARK_HOME to be set"
    exit
fi

status=$(jps|grep -c Master)

if [ $status -gt 0 ]
then
    echo "it seams the spark server is already running"
    echo "found java-process 'Master'"
else
    $SPARK_HOME/sbin/start-master.sh --host 127.0.0.1 --webui-port 8092
    $SPARK_HOME/sbin/start-slave.sh spark://127.0.0.1:7077 
    echo "Started spark"
fi
echo "Check status via browser: http://127.0.0.1:8092/"
