#!/bin/bash

. scripts/_env.sh

function jpsinfo(){
    match=$1
    label=$2
    pid=$(jps -v |grep $match|head -n1|gawk '{print $1}')
    if [ -n $pid ] ; then
	echo "$label ($pid) was started with:"
	ps -p $pid -o command --no-headers |gawk '{print $1}'
    fi
}

if [ -z $JANUS_HOME ] ; then
    echo "need environment var GCALC_HOME to be set"
    exit
fi

if [ -z $GCALC_HOME ] ; then
    echo "need environment var GCALC_HOME to be set"
    exit
fi
   
if [ -z $HADOOP_HOME ] ; then
    echo "need environment var HADOOP_HOME to be set"
    exit
fi

if [ ! -f $GCALC_HOME/scripts/_env.sh ] ; then
    echo "WARNING: GCALC_HOME is not correct"
    echo '  could not find $GCALC_HOME/scripts/_env.sh'
    echo "  with GCALC_HOME =  $GCALC_HOME "
    exit
fi

if [ ! -f $JANUS_HOME/lib/janusgraph-core-$JANUS_VER.jar ] ; then
    echo "ERROR something is wrong with Janusgraph-$JANUS_VER distro"
    exit
fi

if [ ! -f $JANUS_HOME/lib/gcalc-0.0.1.jar ] ; then
    echo "ERROR gcalc-0.0.1.jar is missing in Janusgraph-0.5.2 distro"
    echo "Did you run gcalc_install.sh ? see install.md"
    exit
fi

if [ $(grep -c dfs.replication $HADOOP_HOME/etc/hadoop/hdfs-site.xml) -eq 0 ]
then
    echo "ERROR: need to hadoop_apply_patch.sh"
    exit
fi


if [ $(jps|grep -c Master) -eq 0 ] ; then
    echo "JPS (spark) Master not found "
    exit
else
    if [ "x"$1 == "x-v" ] ; then
	jpsinfo Master Spark-master
    fi
fi
if [ $(jps|grep -c Worker) -eq 0 ] ; then
    echo "JPS (spark) Worker not found"
    exit
fi
if [ $(jps -v |grep -c NameNode) -eq 0 ] ; then
    echo "Hadoop NameNode not found"
    exit
else
    if [ "x"$1 == "x-v" ] ; then
	jpsinfo hadoop.log Hadoop
    fi
fi

if [ "x"$1 == "x-v" ] ; then
    if [ $(jps -v |grep -c janusgraph) -gt 0 ] ; then
	jpsinfo janusgraph Janusgraph
    fi
fi




echo "OK"
