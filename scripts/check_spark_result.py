#!/usr/bin/env python

import sys,math

if(len(sys.argv) < 2):
    print("Usage %s <spark-working-dir>" % sys.argv[0])
    sys.exit()

def get_time_stamp_for(line):
    parts = line.split(" ")
    time = parts[1]
    [h,m,s] = time.split(":")
    hs = int(h)
    ms = int(m)
    ss = int(s)
    # total seconds since 0:00    
    sec = ss + 60*ms + 60*60*hs
    return sec
    
    
sdir = sys.argv[1]
fn =  sdir + '/0/stdout'
with open(fn) as f:
    lines = f.readlines()
    count = len(lines)
    print("line-count : %d " % count)
    
    start = get_time_stamp_for(lines[0])
    print("Started: %s " % start)
    
    finish = get_time_stamp_for(lines[count-2])
    print("Finished: %s " % finish)
    
    seconds_total = finish-start
    mins = math.floor(seconds_total / 60)
    s_mod_60 = seconds_total % 60
    print("Took %s seconds total =  0:%02d:%d " % (seconds_total,mins,s_mod_60))
    print("  = %02d minutes + %d seconds" % (mins,s_mod_60))    
    
    
