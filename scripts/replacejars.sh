#!/bin/bash

if [ $# -lt 1 ] ; then
  cat<<EOF
 Usage:
   -l : list file to be copied
   -r : really do it
EOF
  exit
fi
mode=$1
echo "Replace some jars"
echo "spark: $SPARK_HOME"
echo "janus: $JANUS_HOME"
echo "jars uit spark-distro copieren naar janusgraph..."

for x in  $(ls $JANUS_HOME/lib |grep spark)
do

    y=$SPARK_HOME/jars/$x
    if [ -f $y ] ; then
	if [ $mode == "-l" ] ; then
	    echo $x
	fi
	if [ $mode == "-r" ] ; then
	    cp $y $JANUS_HOME/lib/$x
	fi
    fi
done
if [ $mode == "-r" ] ; then
  # replace scala-library
  rm $JANUS_HOME/lib/scala-library-2.12.10.jar 
  cp $SPARK_HOME/jars/scala-library-2.12.15.jar $JANUS_HOME/lib/
fi

