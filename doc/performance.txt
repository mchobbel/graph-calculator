
Example2 with 4 layers:

example2.4.32 : met 12 cores en 2G memory 8.8 min
example2.4.32 : met 12 cores en 8G memory 4.2 min
example2.4.20 : met 12 cores en 12G memory 0:57  (57 sec)
example2.4.32 : met 12 cores en 12G memory 3:50  (3 m 50)
example2.4.37 : met 12 cores en 12G memory 6:30

2021,  4 Feb
createEx2(hdfs,4,32) met 12 cores en 12G memory 4:24
createEx2(hdfs,4,37) met 12 cores en 12G memory 7:17
createEx2(hdfs,5,18) met 12 cores en 12G memory 7:55


2021 7 Sept
createEx2(hdfs,4,32) met 12 cores en 2G memory 6.3 min

2021 10 sept
createEx2(hdfs,4,32) met 12 cores en 6G memory 4.7 min


Appart, elke iteratie duurt even lang :
 Bij ex2(4,32) zie je deze iteraties 
  Gcalc: convergence-error 33825.0
  Gcalc: convergence-error 33825.0
  Gcalc: convergence-error 1057.0
  Gcalc: convergence-error 33.0
  Gcalc: convergence-error 1.0
   en (met stopwatch) meet ik steeds ong 35 seconden tussen de iteraties,
   terwijl je zou verwachten dat het bij lagere convergence-error sneller gaat.

 
2021 30 sept, nieuwe SSD-drive !
 tweaken van props in hadoop-gson-example2.properties (
      spark.executor.memory
      spark.executor.cores  )
 createEx2(hdfs,4,32)   met cores=12 mem=6g  (1 executor )  4.2 min
 createEx2(hdfs,4,32)   met cores=3 mem=6g   (2 executors)  2.1 min
 createEx2(hdfs,4,32)   met cores=1 mem=6g   (2 executors)  2.8 min
 createEx2(hdfs,4,32)   met cores=1 mem=3g , (4 executors)  1.8 min
   Conclusie: de cores en memory settings zijn quota per executor, dus
    door die omlaag te brengen krijgt spark ruimte om meerdere executors te starten,
    en dat helpt de performance.
    
   
   
 
 
 
