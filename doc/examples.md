# Run the examples

### Simple example

#### Check setup

```
   ./scripts/check_setup.sh
```
The expected output is simply __OK__ , otherwise check [install.md](install.md)

#### Load the configuration
```
    . scripts/_env.sh
```


#### Create input-data
- Open Gremlin-console
```
    run_gremlin.sh
```

- run in the Gremlin-console:

```
    new Gexamples().createEx1(hdfs,'v1')
    hdfs dfs -ls data
```

#### Inspect input-graph

```
   g =  new Ginput(hdfs).ex1('v1')
   g.V().valueMap()
   
```

#### Launch Vertex-program:

- Run the following in the Gremlin-console to launch the Vertex-program in the Spark-cluster

```
    runEx1(hdfs,'v1')
```    
  
#### Check output
- Open Gremlin-console
```
    run_gremlin.sh
```
- Load and inspect the result-data, which is stored in hdfs:output/example1-v1

```
    g =  new Goutput(hdfs).ex1('v1')
    g.V().valueMap()
```


### Bigger example

This example has 4 layers where each parent has 8 children, this totals
± 5000 nodes. ( Nb. feel free to try another values for `layers` or `children` )

#### Generate input

```
    run_gremlin.sh
    createEx2(hdfs,4,8)
```

#### Inspect input-graph

```
   g =  new Ginput(hdfs).ex2(4,8)
   g.V().valueMap()
   
```

#### Run Vertex-program:

```
   runEx2(hdfs,4,8)
```

#### Check output

```
   g =  new Goutput(hdfs).ex2(4,8)
   g.V().valueMap()
   
```
