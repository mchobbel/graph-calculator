## Install Gcalc

#### Check configuration

Inspect the file `scripts/_env.sh`

- `GCALC_HOME` is where this git-repo is checked out.
- `VENDOR_PKGS` is the dir where the 3rd party software will be installed


#### Load the configuration

```
    . scripts/_env.sh
```

#### Install 3rd party software

go to $VENDOR_PKGS , this is where you download + unpack :

- hadoop-2.10.2  [Download](https://archive.apache.org/dist/hadoop/common/)
- spark-3.2.1-bin-hadoop2.7  [Download](https://archive.apache.org/dist/spark/spark-3.2.1/)
- janusgraph-1.0.0-rc1.zip  [Download](https://github.com/JanusGraph/janusgraph/releases/tag/v1.0.0-rc1)

#### Move some jars from spark to janusgraph

This is needed as the spark library packaged in janusgraph is built on a different
version of scala-library.

```
   replacejars.sh -r
```

#### Check ssh public-key

 Hadoop will use 'ssh localhost' which will go easier by using public-key.
 Add your own public rsa-key to localhost authorized_keys
```     
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```
Now try if you can ssh without need for password:
``` 
    ssh localhost 
```

 
 
#### Configure hadoop:
  (for  extra info see  [Setting up a Single Node Cluster](https://hadoop.apache.org/docs/r2.10.0/hadoop-project-dist/hadoop-common/SingleCluster.html) )
```
    hadoop_apply_patch.sh
```


#### Init hdfs filesystem in /tmp/hadoop-$USER + start hadoop
```
    start_hadoop.sh format
    start_hadoop.sh start
    start_hadoop.sh init
```

#### Test client (you should see the 'data' directory listed)
```
    hdfs dfs -ls
```


#### Start spark
```
    start_spark.sh
```




### Compile

#### Compile + install parser
```
 cd antlr
 ./make.sh

```

#### Compile + install the vertex-computer jar

```
    cd $GCALC_HOME
    gcalc_install.sh
```

#### Copy janusgraph libs
```
   rm -rf /var/gcalc/janus_libs/
   cp -r $JANUS_HOME/lib /var/gcalc/janus_libs
```


#### Check hadoop-connection from a Gremlin-console:
```
   run_gremlin.sh
   gremlin>  hdfs
```
 
The expected output is something like
```
      ==>storage[DFS[DFSClient[....
```

Get listing of hadoop home dir
```
     gremlin>  hdfs.ls()
```

Now you should see the data dir :

 ` ==>rwxr-xr-x michiel supergroup 0 (D) data`
