Analyse van hoe het nu werkt,
   zie trace-example1-stdout.txt
   zie example1.drawio tabjes step-1 .. step-5.
   
- Step-2: messages : alle x en y properties
- Step-3: sum1 wordt berekend : SUM(x) op basis van 3 x-waardes.
- Step-4: messages: alle x en y properties + sum1
- Step-5: myresult wordt berekend : DIV(sum1,y)

Wat opvalt :
  - in Step-4 worden x en y opnieuw verstuurd, is dat nodig?
  - in Step-5 wordt SUM(x) niet opnieuw berekend : "found stored-result: 10.60000",
      maar wat als er op een zeker moment iets wijzigt aan de input, bijv er komt een 4e "x" bij,
      hoe weet de vertex dan dat hij toch opnieuw moet rekenen?
        Aha: dat werkt op basis van  input_count en has_fresh_input (status "fresh")
      
  Stel dat we het anders zouden doen, namelijk:
    - Elke message wordt maar 1x verstuurd.
       * De verzender vertex moet dus weten dat ie een bepaalde message al eerder had verstuurd.
    - Inbox krijgt een archief-functie, die dus door de iteraties heen bewaard wordt.
       Nieuwe incoming messages worden toegevoegd aan de Inbox.
       (even kijken hoe dat gaat met storage : kan een vertex ook hashMaps bevatten? )
