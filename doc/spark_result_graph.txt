Hoe zit het met de output-graph van een spark-job?

 De resultaten komen terecht in 256 losse .gryo bestandjes in hdfs.
 # hdfs.ls('output/cql-job/~g')
 ==>rw-r--r-- michiel supergroup 0 part-r-00082
 ==>rw-r--r-- michiel supergroup 0 part-r-00083
 ..
 ..

 Dit is een graph, met alle vertices uit de input-graph zonder Edges.

 Wat kan je daarmee? Je kan deze weer inlezen (in memory):

   conf = new BaseConfiguration()
   conf.setProperty('gremlin.hadoop.inputLocation','output/cql-job')
   conf.setProperty('gremlin.hadoop.graphReader','org.apache.tinkerpop.gremlin.hadoop.structure.io.gryo.GryoInputFormat')
   graph = HadoopGraph.open(conf)
   g = graph.traversal()


 En vervolgens kan je kijken welke target-vars er zijn bijgekomen:
 
   g.V().has('target-var').valueMap(true)
   
   De Vertex-id's zijn identiek tussen input en output. Nice !
   


Q) Is het mogelijk om spark output-graph naar Cql te schrijven ?

A) Nee, janusgraph ondersteunt dit niet (in versie 0.6.0)
   Kijk maar in janusgraph/tree/master/janusgraph-cql/src/main/java/org/janusgraph/hadoop/formats/cql/
    daar vind je wel CqlInputFormat.java maar niet een output format.

   Nb. Wel heb ik het nog geprobeert met deze class:
        org.apache.cassandra.hadoop.cql3.CqlOutputFormat
	
   Door het zo te configureren in hadoop_cql.properties :
     gremlin.hadoop.graphWriter=org.apache.cassandra.hadoop.cql3.CqlOutputFormat
     cassandra.output.keyspace=janus-out
     cassandra.output.partitioner.class=org.apache.cassandra.dht.Murmur3Partitioner
     mapreduce.output.basename=output


   Dan krijg je:
     WARNING:  class org.apache.cassandra.hadoop.cql3.CqlOutputFormat does not implement
       PersistResultGraphAware and thus,  persistence options are unknown.

   Blijkbaar werkt deze class zo dat je zelf een keyspace en een table moet aangeven waar
   key-values in worden opgeslagen, dus geen Gremlin graph data.


Q) Hoe kan je de output graph ophalen? 

A) De output-graph is in Gryo format te vinden in  output/cql-job
   Vanuit die Gryo kan je weer een janusgraph maken.

 Run example1 in Cql-mode (see doc/example1_in_cql.txt )
 

 Open Spark output as 'g2' :
   conf = new BaseConfiguration()
   conf.setProperty('gremlin.hadoop.inputLocation','output/cql-job')
   conf.setProperty('gremlin.hadoop.graphReader','org.apache.tinkerpop.gremlin.hadoop.structure.io.gryo.GryoInputFormat')
   graph2 = HadoopGraph.open(conf)
   g2 = graph2.traversal()


Q) Hoe kan je de output lezen en vertalen naar de original graph?
   - je wilt aan het eind van je VertexProgram-run een lijst hebben met update-statements
      waarmee de moeder graph (de input-graph) wordt bijgewerkt.
      
     * via een gremlin-traversal te doen, zie onder
     * het punt is dan wel dat het duidelijk moet zijn wat er nieuw is bijgekomen (zie example1 v1,v2,v3 )
     * uiteindelijk zou het mooi zijn als dit ook in cluster-mode kan, via een 2e Spark-job

A) Vervolg van bovenstaande (Open spark output as 'g2' )
 
  Create a list of operator-results  [ ID,  KEY, VALUE ]
   alist = g2.V().has('target-var').map( { [it.get().id() , it.get().value('target-var'), it.get().value('operator-result')] }).toList()

  Open Original input graph as 'g1' :
    graph1 = GraphFactory.open('conf/janusgraph-cql.properties')
    g1 = graph1.traversal()

  Store g2 results in g1:
   alist.each {  g1.V(it[0]).property(it[1],it[2]).next() }
   graph1.tx().commit()
   
  Show result:
   g1.V().valueMap()

  
