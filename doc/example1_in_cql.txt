Run Example 1 with cql:

Start Cassandra:
# cd git/qfi_v5/v5.1
# ./scripts/cassan.sh -run


Enter gremlin console:
# run_gremlin.sh


 // Create / open CQL-based graph  (creates keyspace 'janusgraph' if not yet existing)
 //  Nb.  conf/janusgraph-cql.properties is part of janusgraph-distro
 //  Nb.  This is a direct CQL-connection ; not via (spark / hadoop) cluster.
 graph = GraphFactory.open('conf/janusgraph-cql.properties')
 // create input-graph in Cassandra 
 ge = new Gexamples()
 ge.createGraphEx1(graph,'v1')
 

Restart gremlin console with : CTRL-D +
# run_gremlin.sh -skip
 // Create config for Spark
 graph = GraphFactory.open('janusg_conf/hadoop_cql.properties')
 qprog = org.qualifair.gcalc.GcalcVertexProgram.build().inputs("x,y").create()
 result = graph.compute(SparkGraphComputer).program(qprog).submit().get()


Output:
 zie spark_result_graph.txt
 

Open input graph any time:
 graph = GraphFactory.open('conf/janusgraph-cql.properties')
 g = graph.traversal()


