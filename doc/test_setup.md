
Create graph in memory
# graph = GraphFactory.open('conf/janusgraph-inmemory.properties')
# g = graph.traversal()
# g.io('data/grateful-dead.xml').read().iterate()
# g.V().count()


Create small graph in hdfs
# new Gexamples().createEx1(hdfs,'v1')

Open the graph
# g =  new Ginput(hdfs).ex1('v1')

Setup spark-traversal + do spark-based 'count'
# g2 = g.getGraph().traversal().withComputer(SparkGraphComputer)
# g2.V().count()
