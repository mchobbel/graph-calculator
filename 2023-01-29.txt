Spark job wil niet starten wegens
 java.io.InvalidClassException: scala.collection.mutable.WrappedArray$ofRef; local class incompatible: stream classdesc serialVersionUID = 1028182004549731694, local class serialVersionUID = 3456489343829468865

Nb.
 in janusgraph zit:
   spark-core_2.12-3.2.1.jar ( spark-3.2.1  +  scala-2.12) 
   scala-library-2.12.10.jar

 in spark-3.2.1 zit :
   scala-library-2.12.15.jar

Oplossing:
 cp -r ~/gcalc_pkgs/janusgraph-1.0.0-rc1/lib/ /var/gcalc/janus_libs/
 rm /var/gcalc/janus_libs/scala-*

Toevoegen aan example1.properties:
 spark.executor.extraClassPath=/var/gcalc/janus_libs/*



